angular.module('unamore')


.factory('Nieuwsbrief', [ '$http', function ($http) {   'use strict';

  return { 

      getBrieven: function () { 

          var response = $http({   

          url: 'https://unamore.com/admin/cache/nieuwsarchief.json',
          method: "GET",
          params: { 'cachemaster': new Date().getTime() }
        
        }).then(function (response) { console.log(response);
          return response.data; 

        });

          return response; 
      }
  };


}])

.factory('Agenda', [ '$http', function ($http) {   'use strict';

  return { 

      getWeekenden: function () { 

          var response = $http({   

          url: 'https://unamore.com/admin/cache/page_agenda.json',
          method: "GET",
          params: { 'cachemaster': new Date().getTime() }
        
        }).then(function (response) { 
          return response.data; 

        });

          return response; 
      }
  };


}])

.factory('Contact', [ '$http', function ($http) {   'use strict';

  return { 

      getPage: function () { 

          var response = $http({   

          url: 'https://unamore.com/admin/cache/page_contact.json',
          method: "GET",
          params: { 'cachemaster': new Date().getTime() }
        
        }).then(function (response) { 
          return response.data; 

        });

          return response; 
      }
  };


}])


.factory('Serie', [ '$rootScope', '$http', function ($rootScope, $http) {   'use strict';

  return { 

      getSerie: function (serie) {

          var response = $http({   

          url: 'https://unamore.com/admin/cache/' + serie + '.json',
          method: "GET",
          params: { 'cachemaster': new Date().getTime() }
    
        
        }).then(function (response) {

          return response.data;

        });

          return response; 
      }
  };

  }])

.factory('Pagina', [ '$rootScope','$http', function ($rootScope, $http) {  'use strict';

  return { 

      getPage: function (paginaNaam) {

          var response = $http({   

          url: 'https://unamore.com/admin/cache/page_' + paginaNaam + '.json',
          method: "GET", 
          params: { 'cachemaster': new Date().getTime() }
        
        }).then(function (response) {

            console.log(response);

          return response.data;

        });

          return response; 
      }
  };

  }])

.factory('Reacties', [ '$http', function ($http) {  'use strict';

  return { 

      getReacties: function (page) { 

          var response = $http({   

          url: 'https://unamore.com/admin/cache/reacties.json',
          method: "GET",
          params: { 'cachemaster': new Date().getTime() }
        
        }).then(function (response) { 
 
          return response.data;

        });

          return response; 
      }
  };

  }]);