angular.module('unamore')

.directive('fallbackSrc', function () {
  var fallbackSrc = {
    link: function postLink(scope, iElement, iAttrs) {
      iElement.bind('error', function() {
        angular.element(this).attr("src", iAttrs.fallbackSrc);
      });
    }
   }
   return fallbackSrc;
})

.directive('imageonload', [ '$timeout', function($timeout) { 'use strict';
    return { 
        restrict: 'A',
      
        link: function(scope, element) {
          element.on('load', function() {

            angular.element('.spinner').removeClass('spinner-show');
            angular.element('.spinner').addClass('spinner-hide');

          });
        }
    };
}])

.directive('imageLoader', [ '$window', function ($window) { 'use strict';
  return { 
    restrict: 'E',
    templateUrl: '/partials/afbeelding.html',
    scope: {
      loadingIndicatorImage: '@',
      errorImage: '@',
      backgroundImage: '@'
    },
    link: function(scope, element, attrs) {

      angular.element(element[0].querySelector('.spinner')).removeClass('spinner-hide');
      angular.element(element[0].querySelector('.spinner')).addClass('spinner-show');
      angular.element(element[0].querySelector('img')).css("opacity", "0");

      angular.element(element[0].querySelector('img')).bind("load", function() 
      {
        angular.element(element[0].querySelector('.spinner')).removeClass('spinner-show');
        angular.element(element[0].querySelector('.spinner')).addClass('spinner-hide');
        angular.element('#showcase').addClass('move');
        angular.element(element[0].querySelector('img')).css("opacity", "1");
      });
    }
  };
}])

.directive('taalkiezer', [ '$window', function ($window) { 'use strict';

return { 

    restrict: 'E',
    templateUrl: '/partials/taalkiezer.html',
    link: function(scope) { 

        scope.nl = function()
        {
            scope.language = "nl"; 
        };
        scope.en = function()
        {
            scope.language = "en";
        };
        scope.fr = function()
        {
            scope.language = "fr";
        };
        scope.de = function()
        {
            scope.language = "de";
        };
        scope.po = function()
        {
            scope.language = "po";
        };
    }
  };
}])

.directive('klapmenu', [ '$window', '$timeout', function($window, $timeout) {  'use strict';
return { 

    restrict: 'E',
    templateUrl: '/partials/klapmenu.html',
    link: function(scope) {

          var makeMenu = function() { 

            var menulengte = angular.element('#klapmenu').height();

            scope.menu_begin = -menulengte + 160;  // menu gesloten
            scope.menu_eind = 380 - menulengte;  // menu open 
            scope.archief_eind = menulengte + 200; // archief nieuwsbrieven bij menu open 4
            angular.element('#klapmenu').css('top', scope.menu_begin +'px'); 

          };

          makeMenu(); 

          scope.$watch('language', function(newVal, oldVal) { 

              $timeout(function(){ makeMenu(); });

          });

          scope.open_menu = function() 
          { 
              angular.element('#klapmenu').css({'left':'0','top':'50%','-webkit-transform':'translateY(-50%)','-ms-transform':'translateY(-50%)','transform':'translateY(-50%)'});  // 'top': scope.menu_eind +'px', 'left': '0'
              angular.element('#zijbalk_links').css('top', scope.archief_eind +'px');  
              angular.element('.menu-onderkant span').hide();
              angular.element('.serie-scroller').hide();
          };
          scope.close_menu = function() 
          { 
              var menulengte = angular.element('#klapmenu').height() - 140;
              angular.element('#klapmenu').css({'top':'-' + menulengte + 'px', 'left': '-40px','-webkit-transform':'translateY(0%)','-ms-transform':'translateY(0%)','transform':'translateY(0%)'}); 
              angular.element('#zijbalk_links').css('top', '0px'); 
              angular.element('.menu-onderkant span').show();
              angular.element('.serie-scroller').show();
          };

          $timeout(function() {

            if(scope.state.name === 'home') { scope.open_menu(); }

          }, 2000);

          
    }
  };
}])

.directive('serie', [ '$window', function($window) { 'use strict';
return { 

    restrict: 'E',
    templateUrl: '/partials/serie_content.html',
    link: function(scope) {

      scope.close_menu();

      scope.info_blow_up = function() 
      { 
        angular.element('.schilderij-container .schilderij_info').css({'right': '50px', 'opacity':'0.8'}); 
        angular.element('.schilderij-container .schilderij_info .informatie').css({'opacity':'1'}); 
        angular.element('.schilderij-container .serie-scroller .scroll-container').css({'opacity':'1'}); 
        angular.element('.schilderij-container img').css('margin-right', '460px');
        angular.element('.schilderij-container .serie-scroller').css('left', '-500px'); 
        angular.element('.schilderij-container .serie-scroller .click-container').css({'opacity':'1'});
        angular.element('.schilderij-container .schilderij_info .click-container').css({'opacity':'0'}); 
      };

      scope.center = function() 
      { 
        angular.element('.schilderij-container img').css('margin-right', '0px'); 
        angular.element('.schilderij-container .serie-scroller .scroll-container').css({'opacity':'0'}); 
        angular.element('.schilderij-container img').css('margin-left', '0px'); 
        angular.element('.schilderij-container .serie-scroller').css('left', '-360px'); 
        angular.element('.schilderij-container .schilderij_info').css({'right': '-460px', 'opacity':'0.6'});  
        angular.element('.schilderij-container .schilderij_info .informatie').css({'opacity':'0'}); 
        angular.element('.schilderij-container .serie-scroller .click-container').css({'opacity':'1'});
        angular.element('.schilderij-container .schilderij_info .click-container').css({'opacity':'1'}); 
      };

      scope.selectie_blow_up = function() 
      { 
        angular.element('.schilderij-container .serie-scroller').css({'left':'50px', 'opacity':'0.8'}); 
        angular.element('.schilderij-container .serie-scroller .scroll-container').css({'opacity':'1'}); 
        angular.element('.schilderij-container img.schilderij').css('margin-left', '500px'); 
        angular.element('.schilderij-container .schilderij_info').css('right', '-550px'); 
        angular.element('.schilderij-container .schilderij_info .informatie').css({'opacity':'0'}); 
        angular.element('.schilderij-container .serie-scroller .click-container').css({'opacity':'0'}); 
        angular.element('.schilderij-container .schilderij_info .click-container').css({'opacity':'1'});      
      };

    }
  };
}])

.directive('nieuwsbrief', [ '$window', function($window) { 'use strict';

return { 

	restrict: 'E',
	templateUrl: '/partials/nieuwsbrief.html' 

  	};
}])

.directive('archief', [ '$window', function($window) { 'use strict';

return { 

	restrict: 'E',
	templateUrl: '/partials/archief.html' 

  	};
}]);
