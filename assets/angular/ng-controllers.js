(function () {
   'use strict';

    angular.module('unamore')

    .controller('AppCtrl', [ '$scope', '$window', '$rootScope', '$stateParams', '$document', '$timeout', function ($scope, $window, $rootScope, $stateParams, $document, $timeout) {  


      $scope.weburl = $window.location.protocol + '//' + $window.location.host; 

      $rootScope.$on('$stateChangeSuccess',
      function(event, toState, toParams, fromState, fromParams) {
        $scope.state = toState;
      });

      $scope.screen_height = $window.innerHeight; 
      $scope.screen_width = $window.innerWidth; 

      $document.on('scroll', function()
      {
        angular.element('#showcase #arrow_down').css('opacity','0');   
      });

      $scope.initLanguage = function() 
      { 
          $scope.language = "nl";
      };

    }]) 

    .controller('HomeCtrl', [ '$scope', '$timeout', 'Nieuwsbrief', 'Agenda', function ($scope, $timeout, Nieuwsbrief, Agenda) {   

      $scope.init = function() {

        Nieuwsbrief.getBrieven().then(function (data) {
          
          $scope.nieuwsbrief = data.posts[0];

        });
      
        Agenda.getWeekenden().then(function (data) {

          $scope.agenda = data.page;
          $scope.agenda.weekenden = [];

          var count = 0; 
          var item = 0;
          var isOdd = function(x) { return x % 2; };

            angular.forEach($scope.agenda.custom_fields, function(value, key) 
            {
                  if (key.substring(0,8) === "weekend_") {

                    count++; 

                    if ( isOdd(count) === true) 
                    { 
                      $scope.pushdatum = value[0]; 
                    }
                    else 
                    { 
                      var thema = value[0];
                      $scope.agenda.weekenden.push({ datum: $scope.pushdatum, thema: thema });         
                    }

                  }
            });
        });
      };
    }])


    .controller('ContactCtrl', [ '$scope', '$timeout', 'Contact', function ($scope, $timeout, Contact) {   

      $scope.initContact = function() {

        Contact.getPage().then(function (data) 
        { 
          $scope.contact = data.page;
        });
      };
    }])

    .controller('ReactieCtrl', [ '$scope', 'Reacties', function ($scope, Reacties) {   

      $scope.initReactie = function() {

          Reacties.getReacties().then(function (data) {
            $scope.reacties = data.posts;
            $scope.fadein = true;  
          });
      };
    }])

    // .controller('ArchiefCtrl', [ '$scope', '$timeout', 'Nieuwsbrief', 'brief', function ($scope, $timeout, Nieuwsbrief, brief) { 

    

    //   $scope.init = function() {

    //     $scope.slug = brief; 

    //      console.log(brief);

    //       Nieuwsbrief.getBrieven().then(function (data) {
    //         $scope.berichten = data.posts;
    //         $scope.nieuwsbrief = $scope.berichten[0];
    //         $scope.fadein = true;  
    //         angular.forEach($scope.berichten, function(post) 
    //         {  
    //                   // converteer wp datum naar timestamp voor FF 
    //                   var timezone = "Z"; 
    //                   var datum1 = post.date.replace(/\s+/g, "T");
    //                   var datum2 = datum1.concat(timezone);
    //                   var datum3 = datum2.split(/[^0-9]/);
    //                   post.datum = new Date (datum3[0],datum3[1]-1,datum3[2],datum3[3],datum3[4],datum3[5] );
    //                   post.year = datum3[0];
    //         });
    //       });
    //   };

    //   $scope.change = function(index)  
    //     { 
    //       $scope.fadein = false; 
    //       var fadeback = function(){ $scope.fadein = true; };
    //       $timeout( fadeback, [1000]);
    //       var switsj = function(){ $scope.nieuwsbrief = $scope.berichten[index]; };
    //       $timeout( switsj, [750]); 
    //     };
    // }])

    .controller('SerieCtrl', ['$scope', '$stateParams', 'Serie', function ($scope, $stateParams, Serie) {

      $scope.init = function() {

        Serie.getSerie($stateParams.serie).then(function (data) {

          $scope.serie = data.posts;
          $scope.schilderij = $scope.serie[0];

          angular.forEach($scope.serie, function(schilderij) {

              if ($scope.language === 'en' && schilderij.custom_fields.english_title[0].length > 2 ) { schilderij.titel = schilderij.custom_fields.english_title_[0]; schilderij.tekst = schilderij.custom_fields.english_content[0];  }
              else if ($scope.language === 'de' && schilderij.custom_fields.duits_title[0].length > 2 ) { schilderij.titel = schilderij.custom_fields.duits_title[0]; schilderij.tekst = schilderij.custom_fields.duits[0]; } 
              else if ($scope.language === 'po' && schilderij.custom_fields.portugees_titel[0].length > 2) { schilderij.titel = schilderij.custom_fields.portugees_titel[0];schilderij.tekst = schilderij.custom_fields.portugees_content[0]; } 
              else if ($scope.language === 'fr' && schilderij.custom_fields.frans_titel[0].length > 2 ) { schilderij.titel = schilderij.custom_fields.frans_titel[0]; schilderij.tekst = schilderij.custom_fields.frans_content[0]; } 
              else { schilderij.titel = schilderij.title; schilderij.tekst = schilderij.content; }

          });
        
        });

        $scope.$watch('language', function(newVal, oldVal) { 

            if($scope.serie) { 

              angular.forEach($scope.serie, function(schilderij) {

                    if ($scope.language === 'en' && schilderij.custom_fields.english_title[0].length > 2 ) { schilderij.titel = schilderij.custom_fields.english_title_[0]; schilderij.tekst = schilderij.custom_fields.english_content[0];  }
                    else if ($scope.language === 'de' && schilderij.custom_fields.duits_title[0].length > 2 ) { schilderij.titel = schilderij.custom_fields.duits_title[0]; schilderij.tekst = schilderij.custom_fields.duits[0]; } 
                    else if ($scope.language === 'po' && schilderij.custom_fields.portugees_titel[0].length > 2) { schilderij.titel = schilderij.custom_fields.portugees_titel[0];schilderij.tekst = schilderij.custom_fields.portugees_content[0]; } 
                    else if ($scope.language === 'fr' && schilderij.custom_fields.frans_titel[0].length > 2 ) { schilderij.titel = schilderij.custom_fields.frans_titel[0]; schilderij.tekst = schilderij.custom_fields.frans_content[0];  } 
                    else { schilderij.titel = schilderij.title; schilderij.tekst = schilderij.content; } 

              });
            }
        });
      };

      $scope.change = function(index)  
      { 
          angular.element('.spinner').removeClass('spinner-hide');
          angular.element('.spinner').addClass('spinner-show');
          $scope.schilderij = $scope.serie[index];
          $scope.center();
      };

      



    }])

    .controller('PrijzenCtrl', ['$scope', 'Serie', function ($scope, Serie) {

      $scope.init = function(serie) {
      
        Serie.getSerie(serie).then(function (data) {

          $scope.serie = data.posts;        

          angular.forEach($scope.serie, function(schilderij) {

            if ($scope.language === 'en' && schilderij.custom_fields.english_title[0].length > 2 ) { schilderij.title = schilderij.custom_fields.english_title_[0];  }
            else if ($scope.language === 'de' && schilderij.custom_fields.duits_title[0].length > 2 ) { schilderij.title = schilderij.custom_fields.duits_title[0];  } 
            else if ($scope.language === 'po' && schilderij.custom_fields.portugees_titel[0].length > 2) { schilderij.title = schilderij.custom_fields.portugees_titel[0]; } 
            else if ($scope.language === 'fr' && schilderij.custom_fields.frans_titel[0].length > 2 ) { schilderij.title = schilderij.custom_fields.frans_titel[0];  } 

          });
        
        });

      };

      $scope.$watch('language', function(newVal, oldVal) { 

        $scope.fadein = false;

        if($scope.serie !== undefined) 
        {
          angular.forEach($scope.serie, function(schilderij) {
            
            if ($scope.language === 'en' && schilderij.custom_fields.english_title[0].length > 2) { schilderij.title = schilderij.custom_fields.english_title_[0];  }
            else if ($scope.language === 'de' && schilderij.custom_fields.duits_title[0].length > 2 ) { schilderij.title = schilderij.custom_fields.duits_title[0];  } 
            else if ($scope.language === 'po' && schilderij.custom_fields.portugees_titel[0].length > 2) { schilderij.title = schilderij.custom_fields.portugees_titel[0]; } 
            else if ($scope.language === 'fr' && schilderij.custom_fields.frans_titel[0].length > 2 ) { schilderij.title = schilderij.custom_fields.frans_titel[0];  } 
          }); 
        }

        $scope.fadein = true;  

      });

    }])

    .controller('PaginaCtrl', ['$scope', '$stateParams', '$cookies', 'Pagina', function ($scope, $stateParams, $cookies, Pagina) {

      $scope.init = function() 
      {
            // 

            Pagina.getPage($stateParams.titel).then(function (data) {

                console.log(data);

              $scope.pagina = data.page;
              $scope.pagina.titel = []; 
              $scope.pagina.tekst = [];
              

               if ($scope.language === 'en') { $scope.pagina.titel = $scope.pagina.custom_fields.english_title_[0]; $scope.pagina.tekst = $scope.pagina.custom_fields.english_content[0]; }
                else if ($scope.language === 'de') { $scope.pagina.titel = $scope.pagina.custom_fields.duits_title[0]; $scope.pagina.tekst = $scope.pagina.custom_fields.duits[0]; } 
                else if ($scope.language === 'po') { $scope.pagina.titel = $scope.pagina.custom_fields.portugees_titel[0]; $scope.pagina.tekst = $scope.pagina.custom_fields.portugees_content[0]; } 
                else if ($scope.language === 'fr') { $scope.pagina.titel = $scope.pagina.custom_fields.frans_titel[0]; $scope.pagina.tekst = $scope.pagina.custom_fields.frans_content[0]; } 
                else { $scope.pagina.titel = $scope.pagina.title; $scope.pagina.tekst = $scope.pagina.content;  } 

                $scope.fadein = true; 

                 
            });        
      };
      
      $scope.$watch('language', function(newVal, oldVal) { 

        $scope.fadein = false;

        if($scope.pagina !== undefined) 
        {

          if ($scope.language === 'en') { $scope.pagina.titel = $scope.pagina.custom_fields.english_title_[0]; $scope.pagina.tekst = $scope.pagina.custom_fields.english_content[0]; }
          else if ($scope.language === 'de') { $scope.pagina.titel = $scope.pagina.custom_fields.duits_title[0]; $scope.pagina.tekst = $scope.pagina.custom_fields.duits[0]; } 
          else if ($scope.language === 'po') { $scope.pagina.titel = $scope.pagina.custom_fields.portugees_titel[0]; $scope.pagina.tekst = $scope.pagina.custom_fields.portugees_content[0]; } 
          else if ($scope.language === 'fr') { $scope.pagina.titel = $scope.pagina.custom_fields.frans_titel[0]; $scope.pagina.tekst = $scope.pagina.custom_fields.frans_content[0]; } 
          else { $scope.pagina.titel = $scope.pagina.title; $scope.pagina.tekst = $scope.pagina.content;  } 

          $scope.fadein = true; 
        }

      });

    }])       

    .controller('StedenCtrl', ['$scope', '$stateParams', '$cookies', 'Pagina', function ($scope, $stateParams, $cookies, Pagina) {

      $scope.initSteden = function() 
      {
            

            Pagina.getPage('steden').then(function (data) {
              $scope.pagina = data.page;
              $scope.pagina.titel = []; 
              $scope.pagina.tekst = [];
              
               if ($scope.language === 'en') { $scope.pagina.titel = $scope.pagina.custom_fields.english_title_[0]; $scope.pagina.tekst = $scope.pagina.custom_fields.english_content[0]; }
                else if ($scope.language === 'de') { $scope.pagina.titel = $scope.pagina.custom_fields.duits_title[0]; $scope.pagina.tekst = $scope.pagina.custom_fields.duits[0]; } 
                else if ($scope.language === 'po') { $scope.pagina.titel = $scope.pagina.custom_fields.portugees_titel[0]; $scope.pagina.tekst = $scope.pagina.custom_fields.portugees_content[0]; } 
                else if ($scope.language === 'fr') { $scope.pagina.titel = $scope.pagina.custom_fields.frans_titel[0]; $scope.pagina.tekst = $scope.pagina.custom_fields.frans_content[0]; } 
                else { $scope.pagina.titel = $scope.pagina.title; $scope.pagina.tekst = $scope.pagina.content;  } 

                $scope.fadein = true; 
                
            });        
      };
      
      $scope.$watch('language', function(newVal, oldVal) { 

        $scope.fadein = false; 

        if ($scope.language === 'en') { $scope.pagina.titel = $scope.pagina.custom_fields.english_title_[0]; $scope.pagina.tekst = $scope.pagina.custom_fields.english_content[0]; }
        else if ($scope.language === 'de') { $scope.pagina.titel = $scope.pagina.custom_fields.duits_title[0]; $scope.pagina.tekst = $scope.pagina.custom_fields.duits[0]; } 
        else if ($scope.language === 'po') { $scope.pagina.titel = $scope.pagina.custom_fields.portugees_titel[0]; $scope.pagina.tekst = $scope.pagina.custom_fields.portugees_content[0]; } 
        else if ($scope.language === 'fr') { $scope.pagina.titel = $scope.pagina.custom_fields.frans_titel[0]; $scope.pagina.tekst = $scope.pagina.custom_fields.frans_content[0]; } 
        else { $scope.pagina.titel = $scope.pagina.title; $scope.pagina.tekst = $scope.pagina.content;  } 

        $scope.fadein = true; 

      });


    }])  


    .controller('ParijsCtrl', ['$scope', '$stateParams', '$cookies', 'Pagina', function ($scope, $stateParams, $cookies, Pagina) {

      $scope.init = function() 
      {

            Pagina.getPage('Paris').then(function (data) {
              $scope.pagina = data.page;
              $scope.pagina.titel = []; 
              $scope.pagina.tekst = [];
              

               if ($scope.language === 'en') { $scope.pagina.titel = $scope.pagina.custom_fields.english_title_[0]; $scope.pagina.tekst = $scope.pagina.custom_fields.english_content[0]; }
                else if ($scope.language === 'de') { $scope.pagina.titel = $scope.pagina.custom_fields.duits_title[0]; $scope.pagina.tekst = $scope.pagina.custom_fields.duits[0]; } 
                else if ($scope.language === 'po') { $scope.pagina.titel = $scope.pagina.custom_fields.portugees_titel[0]; $scope.pagina.tekst = $scope.pagina.custom_fields.portugees_content[0]; } 
                else if ($scope.language === 'fr') { $scope.pagina.titel = $scope.pagina.custom_fields.frans_titel[0]; $scope.pagina.tekst = $scope.pagina.custom_fields.frans_content[0]; } 
                else { $scope.pagina.titel = $scope.pagina.title; $scope.pagina.tekst = $scope.pagina.content;  } 

                $scope.fadein = true; 

                 
            });        
      };
      
      $scope.$watch('language', function(newVal, oldVal) { 

        $scope.fadein = false; 

        if ($scope.language === 'en') { $scope.pagina.titel = $scope.pagina.custom_fields.english_title_[0]; $scope.pagina.tekst = $scope.pagina.custom_fields.english_content[0]; }
        else if ($scope.language === 'de') { $scope.pagina.titel = $scope.pagina.custom_fields.duits_title[0]; $scope.pagina.tekst = $scope.pagina.custom_fields.duits[0]; } 
        else if ($scope.language === 'po') { $scope.pagina.titel = $scope.pagina.custom_fields.portugees_titel[0]; $scope.pagina.tekst = $scope.pagina.custom_fields.portugees_content[0]; } 
        else if ($scope.language === 'fr') { $scope.pagina.titel = $scope.pagina.custom_fields.frans_titel[0]; $scope.pagina.tekst = $scope.pagina.custom_fields.frans_content[0]; } 
        else { $scope.pagina.titel = $scope.pagina.title; $scope.pagina.tekst = $scope.pagina.content;  } 

        $scope.fadein = true; 

      });

    }]);

}());      