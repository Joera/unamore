(function () {
   'use strict';

angular.module('unamore',
  ['ngAnimate', 'ngCookies',
    'ngTouch', 'ngSanitize',
    'ngResource', 'ui.router','mb-scrollbar'])

.config(function ($stateProvider, $urlRouterProvider, $locationProvider ) { 

    $locationProvider.html5Mode(true).hashPrefix('!');
    
    $urlRouterProvider.otherwise("/");

    $stateProvider

     .state('home', {
        url: '/',
        templateUrl: '/partials/home.html', 
        controller: 'HomeCtrl'
      })

     .state('reacties', {
        url: '/reacties',
        templateUrl: '/partials/reacties.html', 
        controller: 'ReactieCtrl'
     })

     .state('prijzen', {
        url: '/pagina/:titel',
        templateUrl: '/partials/prijzen.html', 
        controller: 'PaginaCtrl'
     })

     .state('nieuwsbrief', {
        url: '/nieuwsbrieven/:brief',
        templateUrl: '/partials/nieuwsbrieven.html', 
        controller: 'ArchiefController', 
        resolve : { brief: ['$stateParams', function($stateParams) { return $stateParams;  }] }
     })

     .state('nieuwsbrieven', {
        url: '/nieuwsbrieven',
        templateUrl: '/partials/nieuwsbrieven.html', 
        controller: 'ArchiefController',
        resolve : { brief: ['$stateParams', function($stateParams) { return 'last';  }] }
     })

     .state('paris', {
        url: '/steden/paris/',  
        templateUrl: '/partials/parijs.html', 
        controller: 'PaginaCtrl'
     })

     .state('steden', {
        url: '/zeven-steden',  
        templateUrl: '/partials/steden.html', 
        controller: 'StedenCtrl'
     })

     .state('pages', {
        url: '/pagina/:titel',
        templateUrl: '/partials/schilden.html', 
        controller: 'PaginaCtrl'
     })

     .state('serie', {
        url: '/serie/:serie',
        templateUrl: '/partials/serie.html', 
        controller: 'SerieCtrl'
     });

});

}());
