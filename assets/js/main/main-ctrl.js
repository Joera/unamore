'use strict';

angular.module('homePage')


.factory('Berichten', [ '$http', function ($http) {   

  return { 

      getPosts: function () { 

          var response = $http({   

          url: "http://unamore.local/api/get_posts/",
          method: "GET"
        
        }).then(function (response) { 
          return response.data;

        });

          return response; 
      }
  }


}])

.controller('HomeCtrl', [ '$scope', '$famous', 'Berichten', function ($scope, $famous, Berichten) {   


          Berichten.getPosts().then(function (data) {

          $scope.berichten = data 
          console.log($scope.berichten); 

          });  

  }]);



