    (function() {
        'use strict';
        angular
            .module('unamore')
            .controller('ArchiefController', ArchiefController);
        
        ArchiefController.$inject = ['$scope', '$timeout', 'Nieuwsbrief', 'brief','$log'];
        /* @ngInject */
        function ArchiefController($scope, $timeout, Nieuwsbrief, brief, $log) {

        $scope.slug = brief; 

        $log.info(brief);
           

        $scope.init = function() {

  

              Nieuwsbrief.getBrieven().then(function (data) {
                $scope.berichten = data.posts;
                $scope.nieuwsbrief = $scope.berichten[0];
                $scope.fadein = true;  
                angular.forEach($scope.berichten, function(post) 
                {  
                          // converteer wp datum naar timestamp voor FF 
                          var timezone = "Z"; 
                          var datum1 = post.date.replace(/\s+/g, "T");
                          var datum2 = datum1.concat(timezone);
                          var datum3 = datum2.split(/[^0-9]/);
                          post.datum = new Date (datum3[0],datum3[1]-1,datum3[2],datum3[3],datum3[4],datum3[5] );
                          post.year = datum3[0];
                });
              });
          };

          $scope.change = function(index)  
            { 
              $scope.fadein = false; 
              var fadeback = function(){ $scope.fadein = true; };
              $timeout( fadeback, [1000]);
              var switsj = function(){ $scope.nieuwsbrief = $scope.berichten[index]; };
              $timeout( switsj, [750]); 
            };


        }
    })();
