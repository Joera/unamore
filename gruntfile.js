module.exports = function(grunt) {
  require('jit-grunt')(grunt);

  grunt.initConfig({
    less: {
      development: {
        options: {
          compress: true,
          strictMath: true,
          sourceMap: true
        },
        files: {
          "./assets/css/main.css": "./assets/less/main.less" // destination file and source file
        }
      }
    },
    watch: {
      styles: {
        files: ['./assets/less/bootstrap/*.less','./assets/less/vendor/*.less','./assets/less/*.less'], // which files to watch
        tasks: ['less'],
        options: {
          nospawn: true
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-less');

  grunt.registerTask('default', ['less', 'watch']);
};